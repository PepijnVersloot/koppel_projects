import sys

def retrieve_input_lines():
    return [line[:-1] for line in sys.stdin.readlines()]

def select_planina(foo):
    int_num = int(foo[0])

    e = pow(2, int_num) + 1
    return pow(e, 2)

input_lines = retrieve_input_lines()
result = select_planina(input_lines)

print(result)
