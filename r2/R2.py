import sys

def retrieve_input_lines ():
    var = [line[:-1] for line in sys.stdin.readlines()]
    spl = var[0].split(" ")
    return spl

def r_2_equation (foo):
    r1 = int(foo[0])
    s = int(foo[1])
    equation = (s * 2) - r1
    return equation

input_lines = retrieve_input_lines()
result = r_2_equation(input_lines)

print(result)
