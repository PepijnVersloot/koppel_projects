import sys

def retrieve_input_lines ():
    return [line[:-1] for line in sys.stdin.readlines()]

def select_quadrant (foo):
    pos_x = int(foo[0])
    pos_y = int(foo[1])

    if pos_x > 0 and pos_y > 0:
        return 1
    elif pos_x < 0 and pos_y > 0:
        return 2
    elif pos_x < 0 and pos_y < 0:
        return 3
    else:
        return 4


input_lines = retrieve_input_lines()
result = select_quadrant(input_lines)

print(result)
