import math

while True:
    print(""" Choose the math opperation.
    0 - Addition
    1 - Subtraction
    2 - Multiplication
    3 - Division
    4 - Modulo
    5 - Raising to a power
    6 - Square root
    7 - Logarithm
    8 - Sine
    9 - Cosine
    10 - Tangent
    """)

    oper = input("Your option from the menu: ")

    # Addition
    if oper == "0":
        val1 = float(input("\nFirst value: "))
        val2 = float(input("Second value: "))

        print("\nThe result is: " + str(val1 + val2) + "\n")

        back = input("\nGo back to the main menu? (y/n)")
        if back == "y":
            continue
        else:
            break

    # Subtraction
    elif oper == "1":
        val1 = float(input("\nFirst value: "))
        val2 = float(input("Second value: "))

        print("\nThe result is: " + str(val1 - val2) + "\n")

        back = input("\nGo back to the main menu? (y/n)")
        if back == "y":
            continue
        else:
            break

    # Multiplication
    elif oper == "2":
        val1 = float(input("\nFirst value: "))
        val2 = float(input("Second value: "))

        print("\nThe result is: " + str(val1 * val2) + "\n")

        back = input("\nGo back to the main menu? (y/n)")
        if back == "y":
            continue
        else:
            break

    # Division
    elif oper == "3":
        val1 = float(input("\nFirst value: "))
        val2 = float(input("Second value: "))

        print("\nThe result is: " + str(val1 / val2) + "\n")

        back = input("\nGo back to the main menu? (y/n)")
        if back == "y":
            continue
        else:
            break

    # Modulo
    elif oper == "4":
        val1 = float(input("\nFirst value: "))
        val2 = float(input("Second value for power: "))

        print("\nThe result is: " + str(val1 % val2) + "\n")

        back = input("\nGo back to the main menu? (y/n)")
        if back == "y":
            continue
        else:
            break

    # Raising to a power
    elif oper == "5":
        val1 = float(input("\nFirst value: "))
        val2 = float(input("Second value: "))

        print("\nThe result is: " + str(math.pow(val1, val2)) + "\n")

        back = input("\nGo back to the main menu? (y/n)")
        if back == "y":
            continue
        else:
            break

    # Square root
    elif oper == "6":
        val1 = float(input("\nEnter value for extraction the square root: "))

        print("\nThe result is: " + str(math.sqrt(val1)) + "\n")

        back = input("\nGo back to the main menu? (y/n)")
        if back == "y":
            continue
        else:
            break

    # Logarithm
    elif oper == "7":
        val1 = float(input("\nEnter value for calculating the logarithm to base two: "))

        print("\nThe result is: " + str(math.log(val1, 2)) + "\n")

        back = input("\nGo back to the main menu? (y/n)")
        if back == "y":
            continue
        else:
            break

    # Sine
    elif oper == "8":
        val1 = float(input("\nEnter value (in degrees) for calculating the sine: "))

        print("\nThe result is: " + str(math.sin(math.radians(val1))) + "\n")

        back = input("\nGo back to the main menu? (y/n)")
        if back == "y":
            continue
        else:
            break

    # Cosine
    elif oper == "9":
        val1 = float(input("\nEnter value (in degrees) for calculating the cosine: "))

        print("\nThe result is: " + str(math.cos(math.radians(val1))) + "\n")

        back = input("\nGo back to the main menu? (y/n)")
        if back == "y":
            continue
        else:
            break

    # Tangent
    elif oper == "10":
        val1 = float(input("\nEnter value (in degrees) for calculating the tangent: "))

        print("\nThe result is: " + str(math.tan(math.radians(val1))) + "\n")

        back = input("\nGo back to the main menu? (y/n)")
        if back == "y":
            continue
        else:
            break

    # Handling invalid options
    else:
        print("\nInvalid option!\n")
        continue

# End of the program
