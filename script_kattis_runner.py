#!./venv/bin/python3
import sys
import requests
import os
import zipfile

# downloading the fil from the internet
problem_name = sys.argv[1]

res = requests.get(f'https://open.kattis.com/problems/{problem_name}/file/statement/samples.zip')

newfile = open(f"{problem_name}.zip", "wb")
newfile.write(res.content)
newfile.close()

# getting the file in the appropriate directory

os.makedirs(problem_name, exist_ok=True)

with zipfile.ZipFile(f"{problem_name}.zip", 'r') as zip_ref:
    zip_ref.extractall(problem_name)

os.remove(f"{problem_name}.zip")

# stop repetition of adding files.


files = os.listdir(f"{problem_name}")

list_ans = []
list_in = []

for i in files:
    if i[-4::] == '.ans':
        list_ans.append(i)
    if i[-3::] == '.in':
        list_in.append(i)

for idx, val in enumerate(sorted(list_ans)):
    os.rename(f"{problem_name}/{val}", f"{problem_name}/{problem_name}.{idx:02d}.ans")

for idx, val in enumerate(sorted(list_in)):
    os.rename(f"{problem_name}/{val}", f"{problem_name}/{problem_name}.{idx:02d}.in")

# printing the directory
file = open(f"{problem_name}/{problem_name}.py", "w+")
file.write(f"""import sys
import os

def retrieve_input_lines():
    return [line[:-1] for line in sys.stdin.readlines()]


def {problem_name}(foo):
    pass


input_lines = retrieve_input_lines()
result = {problem_name}(input_lines)

# print(result)
""")
file.close()
