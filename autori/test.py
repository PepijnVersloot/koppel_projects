from unittest import TestCase
from autori import autori


class AutoriTest(TestCase):
    def test_1(self):
        assert 2 + 2 == 4

    def test_2(self):
        # given
        var = "Knuth-Morris-Pratt"
        # then
        ex = autori(var)
        # expect
        assert ex == "KMP"