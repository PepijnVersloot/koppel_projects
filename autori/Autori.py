import sys

def retrieve_input_lines():
    return [line[:-1] for line in sys.stdin.readlines()]


def autori(line):
    split_line = line.split("-")
    my_string = ""

    for name in split_line:
        my_string += name[0]
    return my_string


input_lines = retrieve_input_lines()
result = autori(input_lines[0])

print(result)
