import sys

def retrieve_input_lines():
    var = [line[:-1] for line in sys.stdin.readlines()]
    return var

def return_line_zero(foo):
    line_zero = foo[0]
    split = line_zero.split(" ")
    return split

input_lines = retrieve_input_lines()
result = return_line_zero(input_lines)

print(result)
